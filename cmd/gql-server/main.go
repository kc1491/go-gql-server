package main

import (
	"bitbucket.com/kc1491/go-gql-server/pkg/server"
)

func main() {
	server.Run()
}
