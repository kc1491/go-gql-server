package main

import (
	"log"

	"bitbucket.com/kc1491/go-gql-server/internal/handlers"
	"github.com/gin-gonic/gin"
)

var HOST, PORT string

func init() {
	HOST = "localhost"
	PORT = "7777"
}

func Run() {
	r := gin.Default()
	// Setup routes
	r.GET("/ping", handlers.Ping())
	log.Println("Running @ http://" + HOST + ":" + PORT)
	log.Fatalln(r.Run(HOST + ":" + PORT))

}
